package com.example.homework

class Work {
    /// Greatest common divisor
    fun GCD(a: Int, b: Int) : Int{
        var gcd = 1
        var i = 1
        while (i <= a && i <= b) {
            if (a % i == 0 && b % i == 0)
                gcd = i
            ++i
        }
        return gcd
    }
    /// Least common multiple
    fun LCM(a: Int, b: Int) : Int{
        var lcm: Int
        if (a > b)
        {lcm = a}
        else{lcm = b}

        while(true){
            if ((lcm % a == 0) && (lcm % b == 0)){
                break
            }
            lcm++
        }
        return lcm
    }
    /// If string contains -> $
    fun string_filter(str : String) : Boolean{
        for (i in str) {
            if ("$" in str){
                return true
            }else{
                return false
            }
        }
        return false
    }
    /// Recursive counter
    fun recursive(num: Int) : Int{
        if (num == 0) {
            return 0
        }else{
            return num + recursive((num - 2))
        }
    }
    /// Reversed number
    fun rev_num(num: Int) : Int{
        var num = num
        var reversed = 0
        while (num > 0) {
            reversed = (reversed * 10 ) + num % 10
            num = num / 10
        }
        return reversed
    }
}

fun main(){
    val gcd = Work()
    println(gcd.GCD(24,40))
    val lcm = Work()
    println(lcm.LCM(7,5))
    val str_filter = Work()
    println(str_filter.string_filter("a"))
    val rec = Work().recursive(100)
    println(rec)
    val rev = Work().rev_num(2322)
    println(rev)
    /// პოლიდომი ვერ გავიგე რა იყო, ვერც ინტერნეტში ვნახე რომელიმე ენაზე განმარტება.
}
