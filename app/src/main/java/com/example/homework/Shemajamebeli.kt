package com.example.homework

class Shemajamebeli {
    fun number_1(masivi : List<Int>) : Int{
        var count = listOf<Int>()
        var count1 = 0
        for (i in masivi){
            if (i in count){
                count1++
            }else{
                count += i
            }
        }
        return count1
    }
    fun number_2(masivi1: List<Int>, masivi2: List<Int>) : List<Int>{
        var new_array = arrayListOf<Int>()
        for (i in masivi1){
            if (i in masivi2){
                new_array.add(i)
            }
        }
        return new_array
    }
    fun number_3(masivi1: List<Int>, masivi2: List<Int>) : List<Int>{
        var new_array = arrayListOf<Int>()
        for (i in masivi1){
            new_array.add(i)
        }
        for (i in masivi2){
            new_array.add(i)
        }
        return new_array
    }
    fun number_4(masivi: List<Int>) : List<Int>{
        var new_array = arrayListOf<Int>()
        var sum = 0
        for (i in masivi){
            sum += i
        }
        var sashualo = sum / masivi.size
        for (i in masivi){
            if (i < sashualo) {
                new_array.add(i)
            }
        }
        return new_array
    }
    fun number_5(masivi: List<Int>) : Int{
        var max = masivi.get(0)
        var scnmax = masivi.get(0)
        for (i in masivi.indices){
            if(masivi.get(i) > max!!){
                scnmax = max
                max = masivi.get(i)
            }
            if (masivi.get(i) > scnmax && masivi.get(i) != max){
                scnmax = masivi.get(i)
            }
        }
        return scnmax
    }
}
fun main(){
    var shemajamebeli = Shemajamebeli()
    var lst = listOf(1,24,5,1,5)
    var lst2 = listOf(1,4,3,24)
    println(shemajamebeli.number_1(lst))
    println(shemajamebeli.number_2(lst,lst2))
    println(shemajamebeli.number_3(lst,lst2))
    println(shemajamebeli.number_4(lst2))
    println(shemajamebeli.number_5(lst2))
}