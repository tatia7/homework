package com.example.homework

class Work2 {
    fun num_to_georgian(num: Int) : String? {
        /// ვქმნით დირექტორის რომელშიც გარკვეული ციფრები შეესაბამებიან გარკვეულ მნიშვნელობას
        val numtowords = mapOf(1 to "ერთი", 2 to "ორი", 3 to "სამი", 4 to "ოთხი", 5 to "ხუთი", 6 to "ექვსი",
        7 to "შვიდი", 8 to "რვა", 9 to "ცხრა", 10 to "ათი", 11 to "თერთმეტი", 12 to "თორმეტი",
        13 to "ცამეტი", 14 to "თოთხმეტი", 15 to "თხუთმეტი", 16 to "თექვსმეტი", 17 to "ჩვიდმეტი",
        18 to "თვრამეტი", 19 to "ცხრამეტი", 20 to "ოცი", 30 to "ოცდაათი", 40 to "ორმოცი", 50 to "ორმოცდაათი",
        60 to "სამოცი", 70 to "სამოცდაათი", 80 to "ოთხმოცი", 90 to "ოთხმოცდაათი",100 to "ასი")
        /// ვქმნი ლისტებს ასებისთვის და ათებისთვის.
        /// რადგან -ასი-ს დამატების შემთვევაში ციფრების დაბოლოების მიხედვით იცვლება შედეგი,
        /// უნდა განვსაზღვროთ, რომელი მთავრდება ა-ზე და რომელი ი-ზე
        /// ათების შემთვევაშიც იგივე, უნდა განვსაზღვროთ დაბოლოება.
        val asebi = listOf("2","3","4","5","6","7")
        val atebi = listOf(30,50,70,90)
        /// პირველ რიგში ვიწყებთ პირდაპირ გამოტანას იმ ციფრების რომლებიც ნაკლებია 20-ზე.
        if (num < 20) {
            return numtowords[num].toString()
        }
        /// შემდეგ "ვთარგმნით" იმ ციფრებს რომლებიც 20..100-ის შუალედშია.
        if (num < 100){
            if (num % 100 == 0){
                return numtowords[num].toString()
            }else{
                var newnum = num.toString().substring(0,num.toString().length - 1) + 0
                var newnum1 = (num.toString().substring(1)).toInt() + 10
                if (newnum.toInt() in atebi){
                    return numtowords[num / 10 * 10]?.replace("ათი","") + numtowords[newnum1]
                 }else {
                    return numtowords[num / 10 * 10]?.replace("ი", "") + "და" + numtowords[num % 10]
                }
            }
        }
        /// საბოლოოდ "ვთარგმნით" 100..1000-ის შუალედში მოქცეულ ციფრებს.
        if (num < 1000){
            if (num == 100){
                return numtowords[num]
            }
            if (num % 100 == 0) {
                if (num.toString().get(0).toString() in asebi){
                    if (num.toString().get(0).toInt() == 7) {
                        return numtowords[num / 100]?.substring(4) + "ასი" + "7"
                    }else{
                        return numtowords[num / 100]?.substring(0, numtowords[num/100].toString().length - 1) + "ასი"
                    }
                }else {
                    return numtowords[num / 100] + "ასი"
                }
            }else {
                if (num.toString().get(0).toString() in asebi) {
                    return numtowords[num / 100]?.substring(0, numtowords[num/100].toString().length - 1) + "ას" + num_to_georgian(num % 100)
                }
                else{
                    return numtowords[num / 100] + "ას" + num_to_georgian(num % 100)
                }
            }
        }
        /// თუ მომხმარებელმა შემოიტანა ჩვენი რეინჯიდან გასული ციფრი დაუბრუნებს ამ წინადადებას.
        return "Can't be converted to word"
    }
}
fun main(){
    print("Enter number - ")
    val number = Integer.valueOf(readLine())
    println(Work2().num_to_georgian(number))

    var num = 356
    var newnum = num.toString()
    println(newnum.get(0).toInt())
}